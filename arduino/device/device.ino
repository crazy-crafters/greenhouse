#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <time.h>
#include <ctime>
#include <iomanip>
#include <string.h>

#include <ArduinoJson.h>

#include <sstream>

#include "core/core.h"

static const IPAddress SSDP_MULTICAST_ADDR(239, 255, 255, 250);

WiFiUDP udp;

void boolFromString(bool& t, const char* value) {
  Serial.print(value);
  if (value == nullptr)
    return;
  Serial.print(value);
  Serial.print("is true ? -> ");
  Serial.println(std::strcmp(value, "true"));
  t = std::strcmp(value, "true") == 0;
}


std::optional<bool> boolFromDTO(const char* value)
{
  if(!value)
    return {};
  bool b;
  boolFromString(b, value);
  return b;
}

std::optional<tm> timeFromDTO(const char* value)
{
  if(!value)
    return {};
  return timeFromString(value);
}

ESP8266WebServer HTTP(80);
HardwareClock realClock;

DeviceCore* core;
void setup()
{
  Serial.begin(115200);
  Serial.println();
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
  Serial.println("\nWaiting for time");
  while (!time(nullptr)) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");

  Serial.println(timeFromString("07:00:00").tm_hour);

  pinMode(0, OUTPUT);
  digitalWrite(0, HIGH); // sets the digital pin 13 on


  core = new DeviceCore(Status{.enabled = true, .start = timeFromString("00:01:00"), .end = timeFromString("23:59:00")}, realClock);
  
  Serial.println("Starting WiFi...");

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  if (WiFi.waitForConnectResult() == WL_CONNECTED) {
    Serial.printf("Starting HTTP...\n");
    HTTP.on("/status", HTTP_GET, []() {
      Serial.printf("getting status!\n");
      HTTP.send(200, "text/json", status().c_str());
    });

    HTTP.on("/configure", HTTP_POST, [](){

      const size_t CAPACITY = JSON_OBJECT_SIZE(1);
      DynamicJsonDocument doc(1024);
      
      Serial.println(HTTP.arg("plain"));

      deserializeJson(doc, HTTP.arg("plain"));      
      JsonObject object = doc.as<JsonObject>();

      Command command{
        .enabled=boolFromDTO(object["enabled"]),
        .start=timeFromDTO(object["start"]),
        .end=timeFromDTO(object["end"])
      };

      core->configure(command);

      HTTP.send ( 200, "text/json", status().c_str() );
  });
    
    HTTP.onNotFound([]() {
      Serial.printf("not found: ");
      Serial.printf(HTTP.uri().c_str());
      Serial.printf("\n");
    });
    HTTP.begin();

    Serial.printf("Starting SSDP...\n");
    udp.beginMulticast(WiFi.localIP(), SSDP_MULTICAST_ADDR, 1900);
    Serial.printf("Ready!\n");
  }
  else {
    Serial.printf("WiFi Failed\n");
    
    while (1) delay(1000);
  }
}


int timer = 0;
void loop()
{
	HTTP.handleClient();
	receiveSSDPDiscoveryRequest();

	delay(100);

  if (timer >= 10) {
    short lightState = LOW;
    if (core->isLightOn())
    {
      lightState = HIGH;
    }
    
    digitalWrite(0, lightState);

    timer = 0;
  } else {
    ++timer;
  }
  
}


int diff(tm const& from, tm const& to) {
  int h1 = from.tm_hour * 3600 + from.tm_min * 60 + from.tm_sec;
  int h2 = to.tm_hour * 3600 + to.tm_min * 60 + to.tm_sec;
  return h1 - h2;
}



std::string status() {
  DynamicJsonDocument doc(1024);
  Status status = core->getStatus();
  doc["enabled"] = status.enabled;
  doc["light-on"] = core->isLightOn();
  doc["start"] = format(status.start);
  doc["end"] = format(status.end);
  doc["now"] = format(core->currentTime());

  std::string content;
  serializeJson(doc, content);
  return content;
}


void setupSSDP() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  udp.beginMulticast(WiFi.localIP(), SSDP_MULTICAST_ADDR, 1900);
  //udp.begin(1900);

  Serial.print(F("UDP Server : ")); Serial.println(WiFi.localIP());
}




void receiveSSDPDiscoveryRequest() {
  int packetSize = udp.parsePacket();
  if (packetSize) {
    char packetBuffer[1024];
    int len = udp.read(packetBuffer, 1024);
    if (len > 0) packetBuffer[len - 1] = 0;
    std::string content = packetBuffer;
    if (content.find("ST: urn:schemas-upnp-org:device:BinaryLight:1") != std::string::npos) {
        std::cout << "found!" << '\n';
        Serial.print(" Received packet from : "); Serial.println(udp.remoteIP());
        Serial.print(" Size : "); Serial.println(packetSize);
        Serial.print(" Data : ");
        Serial.print(packetBuffer);
        udp.beginPacket(udp.remoteIP(), udp.remotePort());
        udp.write("UDP packet was received OK\r\n");
        udp.endPacket();
    } 
  }
}
