#ifndef PLANTS_CORE_H
#define PLANTS_CORE_H

#include <iostream>
#include <iomanip>
#include <optional>


void timeFromString(tm& t, const char* value)
{
    if (value == nullptr)
        return;
    std::istringstream ss(value);
    ss >> std::get_time(&t, "%H:%M:%S");
}

tm timeFromString(const char* value)
{
    tm t{};
    timeFromString(t, value);
    return t;
}

std::string format(const tm& time)
{
    char buffer[ 100 ];
    strftime(buffer, 100, "%H:%M:%S", &time );
    return std::string(buffer);
}

struct Command {
    std::optional<bool> enabled = {};
    std::optional<tm> start = {};
    std::optional<tm> end = {};
};

struct Status {
    bool const enabled;
    tm const start;
    tm const end;

    Status(Status const& other) = default;

    [[nodiscard]] bool inInterval(tm const& now) const
    {
        long fromNowToStart = diff(now, start);
        long fromNowToEnd = diff(now, end);
        if (diff(end, start) >= 0) {
            return (fromNowToStart >= 0 && fromNowToEnd <= 0);
        }
        return (fromNowToEnd <= 0 || fromNowToStart >= 0);
    }

    Status& operator=(Status const& other) {
        if(*this == other) return *this;
        this->~Status();
        return *new(this) Status(other);
    }

    bool operator==(const Status& other) const
    {
        return enabled == other.enabled &&
               start.tm_hour == other.start.tm_hour &&
               start.tm_min == other.start.tm_min &&
               end.tm_hour == other.end.tm_hour &&
               end.tm_min == other.end.tm_min;
    }
private:
    static long diff(tm const& from, tm const& to)
    {
        long h1 = from.tm_hour * 3600 + from.tm_min * 60 + from.tm_sec;
        long h2 = to.tm_hour * 3600 + to.tm_min * 60 + to.tm_sec;
        return h1 - h2;
    }
};


class Clock {
public:
    virtual tm now() const = 0;
};


class HardwareClock: public Clock {
public:
    virtual tm now() const {
        time_t now_t = time(nullptr);
        tm now = *gmtime(&now_t);
        return now;
    }
};


class DeviceCore {
private:
    Status status;
    Clock const& clock;
public:
    DeviceCore(Status const& initial, Clock const& clock): status(initial), clock(clock)
    {}

    [[nodiscard]] bool isLightOn() const
    {
	    return status.enabled && status.inInterval(currentTime());
    }

    [[nodiscard]] Status const& getStatus() const
    {
        return status;
    }

    void configure(Command const& command)
    {
        auto newStatus = Status{
          .enabled = command.enabled.value_or(status.enabled),
          .start = command.start.value_or(status.start),
          .end = command.end.value_or(status.end)
        };
        status = newStatus;
    }

    tm currentTime() const {
	    return clock.now();
    }
};


#endif //PLANTS_CORE_H
