#include "core.h"

#include "pumpkin-test-framework/pumpkintest.h"

inline std::ostream& operator<<(std::ostream& os, Status const& s)
{
    os << "Status(enabled:" << s.enabled << ", start:" << format(s.start) << ", end: " << format(s.end) <<")";
    return os;
}

class FakeClock: public Clock {
private:
    tm time;
public:
    FakeClock(): time({}) {
        timeFromString(time, "12:00:00");
    }
    explicit FakeClock(std::string const& t): time({}) {
        timeFromString(time, t.c_str());
    }
    virtual tm now() const {
        return time;
    }
};

class DeviceCoreTest: public PumpkinTest::AutoRegisteredTestFeature
{
public:
    DeviceCoreTest(): PumpkinTest::AutoRegisteredTestFeature("Device Core Unit Tests")
    {
        test("when disabled, the light should be off", []()
        {
            {
                auto core = DeviceCore(Status{false}, FakeClock());
                PumpkinTest::Assertions::assertEquals(false, core.isLightOn());
            }
        });

        test("given an interval from 07:00 to 20:00, the light should be on at 12:00", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("07:00:00"), .end = timeFromString("20:00:00")}, FakeClock());
                PumpkinTest::Assertions::assertEquals(true, core.isLightOn());
            }
        });

        test("given an interval from 07:00 to 20:00, the light should be off at 05:00", []()
        {
            {
                auto core = DeviceCore(
                        Status{.enabled = true, .start = timeFromString("07:00:00"), .end = timeFromString("20:00:00")},
                        FakeClock("05:00:00"));
                PumpkinTest::Assertions::assertEquals(false, core.isLightOn());
            }
        });

        test("given an interval from 07:00 to 20:00, the light should be off at 23:00", []()
        {
            {
                auto core = DeviceCore(
                        Status{.enabled = true, .start = timeFromString("07:00:00"), .end = timeFromString("20:00:00")},
                        FakeClock("05:00:00")
                        );
                PumpkinTest::Assertions::assertEquals(false, core.isLightOn());
            }
        });

        test("given an interval from 23:00 to 7:00, the light should be on at 06:30", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                       FakeClock("06:30:00")
                           );
                PumpkinTest::Assertions::assertEquals(true, core.isLightOn());
            }
        });

        test("given an interval from 23:00 to 7:00, the light should be on at 23:30", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                       FakeClock("23:30:00")
                );
                PumpkinTest::Assertions::assertEquals(true, core.isLightOn());
            }
        });

        test("given an interval from 23:00 to 7:00, the light should be off at 12:30", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                       FakeClock("12:30:00")
                );
                PumpkinTest::Assertions::assertEquals(false, core.isLightOn());
            }
        });

        test("should give its status", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},FakeClock()
                );
                PumpkinTest::Assertions::assertEquals(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                                      core.getStatus());
            }
        });

        test("should disable light", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                       FakeClock()
                );

                core.configure(Command{.enabled = false});

                PumpkinTest::Assertions::assertEquals(Status{.enabled = false, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                                      core.getStatus());
            }
        });

        test("should change interval", []()
        {
            {
                auto core = DeviceCore(Status{.enabled = true, .start = timeFromString("23:00:00"), .end = timeFromString("07:00:00")},
                                       FakeClock()
                );

                core.configure(Command{
                    .start = timeFromString("08:15:00"),
                    .end = timeFromString("19:20:00")
                });

                PumpkinTest::Assertions::assertEquals(Status{.enabled = true, .start = timeFromString("08:15:00"), .end = timeFromString("19:20:00")},
                                                      core.getStatus());
            }
        });
    }
};

REGISTER_PUMPKIN_TEST(DeviceCoreTest)

int main(int argc, char *argv[])
{
    return PumpkinTest::runAll();
}
