import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.crazycrafters.lightingplants.core.devices.DiscoveryBySSDP
import org.crazycrafters.lightingplants.core.devices.model.Address
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.SocketTimeoutException


class SonosDeviceDiscoveryBySSDPTest {
    @Test
    fun `should send a packet`() {
        val devices = mutableListOf("192.168.0.2", "192.168.0.3", "192.168.0.4", "192.168.0.4")
        val mockedSocket = Mockito.mock(DatagramSocket::class.java)
        Mockito.doAnswer { invocation ->
            if (devices.isEmpty()) {
                throw SocketTimeoutException("stop")
            }
            val args = invocation.arguments
            (args[0] as DatagramPacket).address = InetAddress.getByName(devices.removeFirst())
            null // void method, so return null
        }.`when`(mockedSocket).receive(Mockito.any())

        val detected = mutableListOf<Address>()

        val discoveryBySSDP = DiscoveryBySSDP(mockedSocket)
        runBlocking {
            discoveryBySSDP.discover( { detected.add(it) } ).join()
        }

        assertThat(detected).containsExactlyInAnyOrder(
            Address("192.168.0.2"),
            Address("192.168.0.3"),
            Address("192.168.0.4")
        )
    }
}