package dto

import org.assertj.core.api.Assertions.assertThat
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Status
import org.junit.jupiter.api.Test
import java.time.LocalTime

class DeviceStatusCommandShould {
    @Test
    fun `assert value object equality`() {
        assertThat(
            DeviceStatusCommand(enabled = "true",
                          start = "07:00:00",
                          end = "20:00:00")
        )
        .isEqualTo(
            DeviceStatusCommand(enabled = "true",
                start = "07:00:00",
                end = "20:00:00")
        )

        assertThat(
            DeviceStatusCommand(enabled = "true",
                start = "07:00:00",
                end = "20:00:00").hashCode()
        )
            .isEqualTo(
                DeviceStatusCommand(enabled = "true",
                    start = "07:00:00",
                    end = "20:00:00").hashCode()
        )
    }

    @Test
    fun `assert value object inequality`() {
        assertThat(
            DeviceStatusCommand(enabled = "true",
                start = "07:00:00",
                end = "20:00:00")
        )
            .isNotEqualTo(
                DeviceStatusCommand(enabled = "false",
                    start = "07:00:00",
                    end = "20:00:00")
            )

        assertThat(
            DeviceStatusCommand(enabled = "true",
                start = "07:00:00",
                end = "20:00:00").hashCode()
        )
            .isNotEqualTo(
                DeviceStatusCommand(enabled = "false",
                    start = "07:00:00",
                    end = "20:00:00").hashCode()
            )
    }
}