package org.crazycrafters.lightingplants.core.devices

import org.assertj.core.api.Assertions.assertThat
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.model.Status
import org.junit.jupiter.api.Test
import java.time.LocalTime

class DeviceStatusShould {
    @Test
    fun `assert value object equality`() {
        assertThat(
            DeviceStatus(enabled = true,
                          start = "07:00:00",
                          end = "20:00:00",
                          lightOn = true,
                          now = "12:00:00")
        )
        .isEqualTo(
            DeviceStatus(enabled = true,
                start = "07:00:00",
                end = "20:00:00",
                lightOn = true,
                now = "12:00:00")
        )

        assertThat(
            DeviceStatus(enabled = true,
                start = "07:00:00",
                end = "20:00:00",
                lightOn = true,
                now = "12:00:00")
                .hashCode()
        )
            .isEqualTo(
                DeviceStatus(enabled = true,
                    start = "07:00:00",
                    end = "20:00:00",
                    lightOn = true,
                    now = "12:00:00")
                    .hashCode()
        )
    }

    @Test
    fun `assert value object inequality`() {
        assertThat(
            DeviceStatus(enabled = true,
                start = "07:00:00",
                end = "20:00:00",
                lightOn = true,
                now = "12:00:00")
        )
            .isNotEqualTo(
                DeviceStatus(enabled = false,
                    start = "07:00:00",
                    end = "20:00:00",
                    lightOn = true,
                    now = "12:00:00")
            )

        assertThat(
            DeviceStatus(enabled = true,
                start = "07:00:00",
                end = "20:00:00",
                lightOn = true,
                now = "12:00:00")
                .hashCode()
        )
            .isNotEqualTo(
                DeviceStatus(enabled = true,
                    start = "07:00:00",
                    end = "14:00:00",
                    lightOn = true,
                    now = "12:00:00")
                    .hashCode()
            )
    }
}