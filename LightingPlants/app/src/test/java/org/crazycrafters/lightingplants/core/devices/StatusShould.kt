package model

import org.assertj.core.api.Assertions.assertThat
import org.crazycrafters.lightingplants.core.devices.model.Status
import org.junit.jupiter.api.Test
import java.time.LocalTime

class StatusShould {
    @Test
    fun `assert value object equality`() {
        assertThat(
            Status(enabled = true,
                          start = LocalTime.parse("07:00:00"),
                          end = LocalTime.parse("20:00:00"),
                          lightOn = true,
                          clock =  LocalTime.parse("12:00:00"),
            )
        )
        .isEqualTo(
            Status(enabled = true,
                          start = LocalTime.parse("07:00:00"),
                          end = LocalTime.parse("20:00:00"),
                          lightOn = true,
                          clock =  LocalTime.parse("12:00:00"),)
        )

        assertThat(
            Status(enabled = true,
            start = LocalTime.parse("07:00:00"),
            end = LocalTime.parse("20:00:00"),
                lightOn = true,
                clock =  LocalTime.parse("12:00:00"),).hashCode())
            .isEqualTo(
                Status(enabled = true,
                start = LocalTime.parse("07:00:00"),
                end = LocalTime.parse("20:00:00"),
                    lightOn = true,
                    clock =  LocalTime.parse("12:00:00"),).hashCode())
    }

    @Test
    fun `assert value object inequality`() {
        assertThat(
            Status(enabled = true,
            start = LocalTime.parse("07:00:00"),
            end = LocalTime.parse("20:00:00"),
                lightOn = true,
                clock =  LocalTime.parse("12:00:00"),)
        )
            .isNotEqualTo(
                Status(enabled = false,
                start = LocalTime.parse("07:00:00"),
                end = LocalTime.parse("20:00:00"),
                    lightOn = true,
                    clock =  LocalTime.parse("12:00:00"),)
            )

        assertThat(
            Status(enabled = true,
            start = LocalTime.parse("07:00:00"),
            end = LocalTime.parse("20:00:00"),
                lightOn = true,
                clock =  LocalTime.parse("12:00:00"),).hashCode())
            .isNotEqualTo(
                Status(enabled = true,
                start = LocalTime.parse("17:00:00"),
                end = LocalTime.parse("20:00:00"),
                    lightOn = true,
                    clock =  LocalTime.parse("12:00:00"),).hashCode())
    }
}