package org.crazycrafters.lightingplants.core.devices.model

import org.assertj.core.api.Assertions.assertThat
import org.crazycrafters.lightingplants.core.devices.model.Address
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.core.devices.model.Status
import org.junit.jupiter.api.Test
import java.time.LocalTime

class DeviceShould {
    @Test
    fun `assert entity equality`() {
        assertThat(
            the_device(2, enabled = true)
        ).isEqualTo(
            the_device(2, enabled = false)
        )

        assertThat(
            the_device(2, enabled = true).hashCode()
        ).isEqualTo(
            the_device(2, enabled = false).hashCode()
        )
    }

    @Test
    fun `assert entity inequality`() {
        assertThat(
            the_device(2, enabled = true)
        ).isNotEqualTo(
            the_device(3, enabled = true)
        )

        assertThat(
            the_device(2, enabled = true).hashCode()
        ).isNotEqualTo(
            the_device(3, enabled = true).hashCode()
        )
    }
}


fun the_device(id: Int, enabled: Boolean = true)
    = Device(Address("192.168.1.$id"),
            status = Status(
                enabled = enabled,
                start = LocalTime.parse("07:00:00"),
                end = LocalTime.parse("20:00:00"),
                lightOn = true,
                clock = LocalTime.parse("12:00:00"),
            )
    )