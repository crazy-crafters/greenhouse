import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.crazycrafters.lightingplants.core.devices.*
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatusCommand
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Address
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.core.devices.model.Status
import org.junit.jupiter.api.Test
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.DatagramSocket
import java.time.LocalTime

class DiscoveryFake: DeviceDiscovery {
    override fun discover(onDiscover: (Address) -> Unit): Job {
        return runBlocking {
            return@runBlocking launch {
                onDiscover(Address("192.168.0.2"))
                onDiscover(Address("192.168.0.3"))
                onDiscover(Address("192.168.0.4"))
            }
        }

    }
}

class DeviceRepositoryTest {
    val status = DeviceStatus(enabled = false,
        start = "07:00:00",
        end = "20:00:00",
        lightOn = true,
        now = "12:00:00")

    @Test
    fun `should notify for each device`() {
        val all = mutableListOf<Device>()
        runBlocking {
            val ssdp = DiscoveryFake()

            val repository = DeviceRepository(ssdp) {
                object : DeviceClient {
                    override fun status(): DeviceStatus {
                        return status
                    }

                    override fun configure(
                        status: DeviceStatusCommand,
                        onSuccess: (Status) -> Unit
                    ) {
                        TODO("Not yet implemented")
                    }
                }
            }

            repository.queryAll {
                all.add(it)
            }.join()
        }

        assertThat(all).containsExactlyInAnyOrder(
            Device(Address("192.168.0.2"), Status(enabled = true, start = LocalTime.parse("07:00:00"), end= LocalTime.parse("20:00:00"), lightOn = true,
                          clock = LocalTime.parse("12:00:00"),)),
            Device(Address("192.168.0.3"), Status(enabled = true, start = LocalTime.parse("07:00:00"), end= LocalTime.parse("20:00:00"), lightOn = true,
                          clock = LocalTime.parse("12:00:00"),)),
            Device(Address("192.168.0.4"), Status(enabled = true, start = LocalTime.parse("07:00:00"), end= LocalTime.parse("20:00:00"), lightOn = true,
                          clock = LocalTime.parse("12:00:00"),))
        )
    }

    @Test
    fun `should send configuration to a device`() {
        lateinit var commandSent: DeviceStatusCommand

        val repository = DeviceRepository(DiscoveryFake()) {
            object : DeviceClient {
                override fun status(): DeviceStatus {
                    return status
                }

                override fun configure(status: DeviceStatusCommand, onSuccess: (Status) -> Unit) {
                    commandSent = status
                }
            }
        }

        val device = Device(
            Address("192.168.0.2"),
            Status(enabled = true,
                   start = LocalTime.parse("07:00:00"),
                   end= LocalTime.parse("20:00:00"),
                   lightOn = true,
                   clock = LocalTime.parse("12:00:00"),))
        repository.configure(
                            device,
                            StatusCommand(
                                enabled = false,
                                start = LocalTime.parse("08:30:00"),
                                end = LocalTime.parse("22:15:00")),
                            onSuccess = {}
        )

        assertThat(
            commandSent
        ).isEqualTo(
            DeviceStatusCommand(enabled = "false",
                                start = "08:30:00",
                                end = "22:15:00")
        )
    }
}