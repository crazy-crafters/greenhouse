package org.crazycrafters.lightingplants.core.devices

import kotlinx.coroutines.Job
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.core.devices.model.Status

interface FindDevices {
    fun queryAll(whenFound: (Device) -> Unit): Job
    fun configure(device: Device, command: StatusCommand, onSuccess: (Status) -> Unit)
}