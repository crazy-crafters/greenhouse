package org.crazycrafters.lightingplants.core.devices

import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatusCommand
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface DeviceRestClient {
    @GET("/status")
    fun status(): Call<DeviceStatus>

    @POST("/configure")
    fun configure(@Body status: DeviceStatusCommand): Call<DeviceStatus>
}


