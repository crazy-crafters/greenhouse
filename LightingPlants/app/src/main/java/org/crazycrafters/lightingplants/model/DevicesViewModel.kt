package org.crazycrafters.lightingplants.model

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SnapshotMutationPolicy
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Address
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.core.devices.model.Status
import java.time.LocalTime
import java.util.Timer
import java.util.TimerTask
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.O)
@HiltViewModel
class DevicesViewModel @Inject constructor(): ViewModel() {
    /*
        Device(Address("192.168.1.10"), Status(enabled = true, start = LocalTime.parse("07:00:00"), end = LocalTime.parse("20:00:00"), lightOn = true, clock = LocalTime.now())),
        Device(Address("192.168.1.11"), Status(enabled = false, start = LocalTime.parse("08:30:00"), end = LocalTime.parse("20:00:00"), lightOn = false, clock = LocalTime.parse("12:00:00"))),
        Device(Address("192.168.1.12"), Status(enabled = false, start = LocalTime.parse("09:00:00"), end = LocalTime.parse("19:30:00"), lightOn = true, clock = LocalTime.parse("12:00:00"))),
        Device(Address("192.168.1.13"), Status(enabled = true, start = LocalTime.parse("07:15:00"), end = LocalTime.parse("20:57:00"), lightOn = false, clock = LocalTime.parse("23:00:00"))),
    */

    var devices by mutableStateOf(listOf<Device>(), policy = Policy())
        private set

    val repository = Devices()

    fun update(device: Device, command: StatusCommand) {
        repository.configure(device, command) { updatedStatus ->
            try {
                val updatedDevice = device.copy(status = updatedStatus)
                add(updatedDevice)
            } catch (e: Throwable) {
                Log.e("ERD", e.toString())
            }
        }
    }

    private fun add(device: Device) {
        viewModelScope.launch(Dispatchers.IO) {
            devices = (devices.filter { it.address != device.address } + device).sortedBy { it.address.value  }
            Log.i("ERD0", device.status.toString())
        }
    }

    init {
        repository.queryAll { device ->
            add(device)
        }
    }
}

class Policy : SnapshotMutationPolicy<List<Device>> {
    override fun equivalent(a: List<Device>, b: List<Device>): Boolean {
        return a.map { it.address to it.status }.containsAll(b.map { it.address to it.status })
    }

}