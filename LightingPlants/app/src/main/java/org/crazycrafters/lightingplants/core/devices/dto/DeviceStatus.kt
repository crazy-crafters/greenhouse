package org.crazycrafters.lightingplants.core.devices.dto

import com.google.gson.annotations.SerializedName

data class DeviceStatus(val enabled: Boolean, val start: String, val end: String, @SerializedName("light-on") val lightOn: Boolean, val now: String)
