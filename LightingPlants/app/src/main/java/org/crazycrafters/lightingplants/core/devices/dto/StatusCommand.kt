package org.crazycrafters.lightingplants.core.devices.dto

import java.time.LocalTime

data class StatusCommand(val enabled: Boolean? = null, val start: LocalTime? = null, val end: LocalTime? = null)