package org.crazycrafters.lightingplants.core.devices

import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.Job
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatusCommand
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Address
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.core.devices.model.Status
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@RequiresApi(Build.VERSION_CODES.O)
class DeviceRepository(private val deviceDiscovery: DeviceDiscovery, private val restClientFor: (Address) -> DeviceClient): FindDevices {
    override fun queryAll(whenFound: (Device) -> Unit): Job {
        return deviceDiscovery.discover {
            val service = restClientFor(it)
            val device = Device(it, service.status().toDomain())
            whenFound(device)
        }
    }
    override fun configure(device: Device, command: StatusCommand, onSuccess: (Status) -> Unit) {
        val service = restClientFor(device.address)
        service.configure(command.toDTO(), onSuccess)
    }

    private fun StatusCommand.toDTO()
    = DeviceStatusCommand(this.enabled?.toString(),
                          this.start?.format(DateTimeFormatter.ISO_TIME),
                          this.end?.format(DateTimeFormatter.ISO_TIME))
}

@RequiresApi(Build.VERSION_CODES.O)
fun DeviceStatus.toDomain(): Status {
    return Status(
            enabled = enabled,
            start = LocalTime.parse(start),
            end = LocalTime.parse(end),
            lightOn = lightOn,
            clock = LocalTime.parse(now),
    )
}