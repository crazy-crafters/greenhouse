package org.crazycrafters.lightingplants.core.devices.dto

data class DeviceStatusCommand(val enabled: String? = null, val start: String? = null, val end: String? = null)

