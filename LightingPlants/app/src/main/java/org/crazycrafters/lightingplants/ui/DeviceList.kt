package org.crazycrafters.lightingplants.ui

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.model.DevicesViewModel

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DeviceList(
    viewModel: DevicesViewModel = hiltViewModel()
) {
    val devices = viewModel.devices

    Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
        devices.forEach { device ->
            DeviceCard(device) { device: Device, command: StatusCommand ->
                viewModel.update(device, command)
            }
        }
    }
}