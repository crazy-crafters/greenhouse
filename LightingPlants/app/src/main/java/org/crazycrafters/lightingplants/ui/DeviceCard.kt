package org.crazycrafters.lightingplants.ui

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ButtonColors
import androidx.compose.material.TextButton
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathFillType
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.path
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.chargemap.compose.numberpicker.*
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Device
import java.time.LocalTime

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DeviceCard(device: Device, update: (Device, StatusCommand) -> Unit) {
    var openAlertDialog by remember { mutableStateOf(false) }

    if(openAlertDialog) {
        AlertDialogExample(
            onClose = { openAlertDialog = false },
            onConfirmation = update,
            device = device
        )
    }


    Card(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surface,
        ),
        border = BorderStroke(1.dp, Color.Black),
        modifier =  Modifier.padding(all = 16.dp)
    ) {
        Column(modifier = Modifier.padding(horizontal = 32.dp, vertical = 24.dp)) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                /*Icon(imageVector =  if(device.status.malfunction) {
                                        skull()
                                    } else if (device.status.lightOn){
                                        Icons.Filled.Lightbulb
                                    } else {
                                        Icons.Filled.Light
                                    },
                    contentDescription = "",
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colorScheme.tertiary
                )*/
                Text(
                    device.address.value,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.W700,
                    modifier = Modifier.padding(10.dp)
                )
                Switch(
                    checked = device.status.enabled,
                    onCheckedChange = { update(device, StatusCommand(enabled = it)) }
                )
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                /*IconButton(
                    onClick = { openAlertDialog = true },
                ) {
                    Icon(imageVector = Icons.Filled.Settings,
                        contentDescription = "",
                        modifier = Modifier.size(48.dp),
                        tint = MaterialTheme.colorScheme.primaryContainer)
                }*/
                Button(
                    onClick = { openAlertDialog = true },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.secondaryContainer,
                        contentColor = MaterialTheme.colorScheme.onBackground)
                ) {
                    Text(
                        text = "${device.period}",
                        fontSize = 20.sp,
                        fontWeight = FontWeight.W400,
                        //modifier = Modifier.padding(10.dp)
                    )
                }
            }
        }
    }
}


@Composable
fun AlertDialogExample(
    onClose: () -> Unit,
    onConfirmation: (Device, StatusCommand) -> Unit,
    device: Device
) {

    var startValue by remember { mutableStateOf<Hours>(FullHours(device.status.start.hour, device.status.start.minute)) }
    var endValue by remember { mutableStateOf<Hours>(FullHours(device.status.end.hour, device.status.end.minute)) }

    AlertDialog(
        title = {
            Text(text = device.address.value)
        },
        text = {
            Column {
                Text("Turn on the light at")
                HoursNumberPicker(
                    dividersColor = MaterialTheme.colorScheme.tertiaryContainer,
                    textStyle = TextStyle.Default.copy(color = Color.White),
                    leadingZero = false,
                    value = startValue,
                    onValueChange = {
                        startValue = it
                    },
                    hoursDivider = {
                        Text(
                            modifier = Modifier.size(24.dp),
                            textAlign = TextAlign.Center,
                            text = ":"
                        )
                    }
                )
                Text("Turn off the light at")
                HoursNumberPicker(
                    dividersColor = MaterialTheme.colorScheme.tertiaryContainer,
                    textStyle = TextStyle.Default.copy(color = Color.White),
                    leadingZero = false,
                    value = endValue,
                    onValueChange = {
                        endValue = it
                    },
                    hoursDivider = {
                        Text(
                            modifier = Modifier.size(24.dp),
                            textAlign = TextAlign.Center,
                            text = ":"
                        )
                    }
                )
            }
        },
        onDismissRequest = {
            onClose()
        },
        confirmButton = {
            TextButton(
                onClick = {
                    val newStart = LocalTime.of(startValue.hours, startValue.minutes)
                    val newEnd = LocalTime.of(endValue.hours, endValue.minutes)
                    onConfirmation(device, StatusCommand(start = newStart, end = newEnd))
                    onClose()
                }
            ) {
                Text("Confirm")
            }
        },
        dismissButton = {
            TextButton(
                onClick = {
                    onClose()
                }
            ) {
                Text("Dismiss")
            }
        }
    )
}

@Composable
fun skull(): ImageVector {
    return remember {
        ImageVector.Builder(
            name = "skull",
            defaultWidth = 40.0.dp,
            defaultHeight = 40.0.dp,
            viewportWidth = 40.0f,
            viewportHeight = 40.0f
        ).apply {
            path(
                fill = SolidColor(Color.Black),
                fillAlpha = 1f,
                stroke = null,
                strokeAlpha = 1f,
                strokeLineWidth = 1.0f,
                strokeLineCap = StrokeCap.Butt,
                strokeLineJoin = StrokeJoin.Miter,
                strokeLineMiter = 1f,
                pathFillType = PathFillType.NonZero
            ) {
                moveTo(10.083f, 36.583f)
                verticalLineToRelative(-7.041f)
                quadToRelative(-1.541f, -0.667f, -2.771f, -1.813f)
                quadToRelative(-1.229f, -1.146f, -2.104f, -2.625f)
                reflectiveQuadToRelative(-1.333f, -3.208f)
                quadToRelative(-0.458f, -1.729f, -0.458f, -3.563f)
                quadToRelative(0f, -6.541f, 4.645f, -10.75f)
                quadTo(12.708f, 3.375f, 20f, 3.375f)
                quadToRelative(7.292f, 0f, 11.958f, 4.208f)
                quadToRelative(4.667f, 4.209f, 4.667f, 10.75f)
                quadToRelative(0f, 1.834f, -0.479f, 3.563f)
                reflectiveQuadToRelative(-1.354f, 3.208f)
                quadToRelative(-0.875f, 1.479f, -2.104f, 2.625f)
                quadToRelative(-1.23f, 1.146f, -2.73f, 1.813f)
                verticalLineToRelative(7.041f)
                close()
                moveToRelative(2.625f, -2.625f)
                horizontalLineToRelative(2.667f)
                verticalLineToRelative(-3.916f)
                horizontalLineTo(18f)
                verticalLineToRelative(3.916f)
                horizontalLineToRelative(4.042f)
                verticalLineToRelative(-3.916f)
                horizontalLineToRelative(2.625f)
                verticalLineToRelative(3.916f)
                horizontalLineToRelative(2.625f)
                verticalLineToRelative(-6.083f)
                quadToRelative(1.541f, -0.458f, 2.791f, -1.354f)
                quadToRelative(1.25f, -0.896f, 2.105f, -2.146f)
                quadToRelative(0.854f, -1.25f, 1.312f, -2.792f)
                quadToRelative(0.458f, -1.541f, 0.458f, -3.25f)
                quadToRelative(0f, -5.5f, -3.854f, -8.895f)
                quadTo(26.25f, 6.042f, 20f, 6.042f)
                quadToRelative(-6.25f, 0f, -10.104f, 3.396f)
                quadToRelative(-3.854f, 3.395f, -3.854f, 8.895f)
                quadToRelative(0f, 1.709f, 0.458f, 3.25f)
                quadToRelative(0.458f, 1.542f, 1.312f, 2.792f)
                quadToRelative(0.855f, 1.25f, 2.105f, 2.146f)
                quadToRelative(1.25f, 0.896f, 2.791f, 1.354f)
                close()
                moveToRelative(4.917f, -9.041f)
                horizontalLineToRelative(4.75f)
                lineTo(20f, 20.125f)
                close()
                moveToRelative(-3.458f, -3.875f)
                quadToRelative(1.25f, 0f, 2.125f, -0.875f)
                reflectiveQuadToRelative(0.875f, -2.125f)
                quadToRelative(0f, -1.209f, -0.875f, -2.104f)
                quadToRelative(-0.875f, -0.896f, -2.125f, -0.896f)
                reflectiveQuadToRelative(-2.105f, 0.896f)
                quadToRelative(-0.854f, 0.895f, -0.854f, 2.104f)
                quadToRelative(0f, 1.25f, 0.875f, 2.125f)
                reflectiveQuadToRelative(2.084f, 0.875f)
                close()
                moveToRelative(11.666f, 0f)
                quadToRelative(1.25f, 0f, 2.125f, -0.875f)
                reflectiveQuadToRelative(0.875f, -2.125f)
                quadToRelative(0f, -1.209f, -0.875f, -2.104f)
                quadToRelative(-0.875f, -0.896f, -2.125f, -0.896f)
                reflectiveQuadToRelative(-2.104f, 0.896f)
                quadToRelative(-0.854f, 0.895f, -0.854f, 2.104f)
                quadToRelative(0f, 1.25f, 0.875f, 2.125f)
                reflectiveQuadToRelative(2.083f, 0.875f)
                close()
                moveTo(20f, 33.958f)
                close()
            }
        }.build()
    }
}