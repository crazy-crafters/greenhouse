package org.crazycrafters.lightingplants.core.devices

import android.util.Log
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.crazycrafters.lightingplants.core.devices.model.Address
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.SocketTimeoutException
import java.time.LocalDateTime


class DiscoveryBySSDP(private val clientSocket: DatagramSocket): DeviceDiscovery {

    private var lastDiscovery: LocalDateTime = LocalDateTime.MIN

    @OptIn(DelicateCoroutinesApi::class)
    override fun discover(onDiscover: (Address) -> Unit): Job {
        return GlobalScope.launch {
            scan (onDiscover,
                onFinish = {
                    lastDiscovery = it
                })
        }
    }

    private fun scan(onDiscover: (Address) -> Unit, onFinish: (LocalDateTime) -> Unit) {
        val msearch =
            "M-SEARCH * HTTP/1.1\r\nHost: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nST: urn:schemas-upnp-org:device:BinaryLight:1\r\nMX: 1\r\n"
        val receiveData = ByteArray(1024)
        val sendData = msearch.toByteArray()
        val sendPacket = DatagramPacket(
            sendData,
            sendData.size,
            InetAddress.getByName("239.255.255.250"),
            1900
        )

        clientSocket.soTimeout = 10000


        Log.w("ERD0", "Scanning...")
        clientSocket.send(sendPacket)

        val knownAddresses = mutableListOf<Address>()

        while (true) {
            try {
                val receivePacket = DatagramPacket(receiveData, receiveData.size)
                clientSocket.receive(receivePacket)
                val address = Address(receivePacket.address.hostAddress)
                Log.w("ERD0", "Receiving from $address")
                if(knownAddresses.contains(address))
                    continue
                onDiscover(address)
                knownAddresses.add(address)
            } catch (e: SocketTimeoutException) {
                break
            }
        }
        clientSocket.close()
        onFinish(LocalDateTime.now())
        Log.w("ERD0", "Scan done.")
    }
}
