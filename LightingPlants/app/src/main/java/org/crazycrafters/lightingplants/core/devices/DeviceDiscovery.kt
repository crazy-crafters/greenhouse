package org.crazycrafters.lightingplants.core.devices

import kotlinx.coroutines.Job
import org.crazycrafters.lightingplants.core.devices.model.Address

interface DeviceDiscovery {
    fun discover(onDiscover: (Address) -> Unit): Job
}