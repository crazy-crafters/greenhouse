package org.crazycrafters.lightingplants.core.devices.model

data class Address(val value: String)