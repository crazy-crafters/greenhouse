package org.crazycrafters.lightingplants.core.devices.model

import java.time.LocalTime

data class Status(val enabled: Boolean, val start: LocalTime, val end: LocalTime, val lightOn: Boolean, val clock: LocalTime) {

    val malfunction = (clock.isAfter(start) && clock.isBefore(end)) != lightOn
}