package org.crazycrafters.lightingplants.core.devices.model

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.Duration
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@RequiresApi(Build.VERSION_CODES.O)
data class Device(val address: Address, val status: Status) {
    override fun equals(other: Any?): Boolean {
        return other is Device && address == other.address
    }

    override fun hashCode(): Int {
        return address.hashCode()
    }

    val period: Period
        get() = Period(status.start, status.end)
}

@RequiresApi(Build.VERSION_CODES.O)
data class Period(val start: LocalTime, val end: LocalTime) {
    override fun toString(): String {
        return "${start.format(DateTimeFormatter.ofPattern("HH:mm"))} - ${
            end.format(
                DateTimeFormatter.ofPattern("HH:mm")
            )
        } (${duration()})"
    }

    private fun duration(): String {
        val d = Duration.between(start, end)
        return "${d.toHours().toString().padStart(2, '0')}h${(d.toMinutes() % 60).toString().padStart(2, '0')}"
    }
}