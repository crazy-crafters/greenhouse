package org.crazycrafters.lightingplants.core.devices

import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Status

interface DeviceClient {
    fun status(): DeviceStatus
    fun configure(status: DeviceStatusCommand, onSuccess: (Status) -> Unit)
}