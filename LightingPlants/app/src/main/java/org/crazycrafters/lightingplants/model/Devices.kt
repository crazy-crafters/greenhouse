package org.crazycrafters.lightingplants.model

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.crazycrafters.lightingplants.core.devices.DeviceClient
import org.crazycrafters.lightingplants.core.devices.DeviceRepository
import org.crazycrafters.lightingplants.core.devices.DeviceRestClient
import org.crazycrafters.lightingplants.core.devices.DiscoveryBySSDP
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatus
import org.crazycrafters.lightingplants.core.devices.dto.DeviceStatusCommand
import org.crazycrafters.lightingplants.core.devices.dto.StatusCommand
import org.crazycrafters.lightingplants.core.devices.model.Address
import org.crazycrafters.lightingplants.core.devices.model.Device
import org.crazycrafters.lightingplants.core.devices.model.Status
import org.crazycrafters.lightingplants.core.devices.toDomain
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.DatagramSocket
import java.time.LocalTime

@RequiresApi(Build.VERSION_CODES.O)
class Devices {
    val ssdp = DiscoveryBySSDP(DatagramSocket())

    val repository = DeviceRepository(ssdp) {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://${it.value}")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(DeviceRestClient::class.java)

        object : DeviceClient {
            override fun status() = service.status().execute().body()!!
            override fun configure(status: DeviceStatusCommand, onSuccess: (Status) -> Unit) {
                service.configure(status).enqueue(object : Callback<DeviceStatus> {
                    override fun onResponse(
                        call: Call<DeviceStatus>,
                        response: Response<DeviceStatus>
                    ) {
                        onSuccess(response.body()?.toDomain()!!)
                    }

                    override fun onFailure(call: Call<DeviceStatus>, t: Throwable) {
                        Log.w("ERD", t)
                    }
                })
            }

        }
    }

    fun queryAll(f: (Device) -> Unit) {
        repository.queryAll(f)
    }

    fun configure(device: Device, command: StatusCommand, onSuccess: (Status) -> Unit) {
        repository.configure(device, command, onSuccess)
    }
}